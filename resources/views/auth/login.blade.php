@extends('layouts.app')

@section('content')

<div class="title__wrapper">
    <h1 class="title title_size-l">Sign in</h1>
</div>

<form class="form" method="POST" action="{{ route('login') }}">
    @csrf

    <label for="email" class="form__label">{{ __('Email Address') }}</label>
    <div class="form__input-wrapper">
        <input id="email" type="email" class="form__input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

        @error('email')
            <div class="form__message-wrapper">
                <span class="form__message" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            </div>
        @enderror
    </div>

    <label for="password" class="form__label">{{ __('Password') }}</label>
    <div class="form__input-wrapper">
        <input id="password" type="password" class="form__input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

        @error('password')
            <div class="form__message-wrapper">
                <span class="form__message" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            </div>
        @enderror
    </div>

    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
    <label class="form__label _checkbox" for="remember">
        {{ __('Remember Me') }}
    </label>

    <div class="form__btn-wrapper">
        <button type="submit" class="link submit-btn">
            {{ __('Login') }}
        </button>
    </div>

    <div class="form__label">
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
    </div>
</form>
@endsection
