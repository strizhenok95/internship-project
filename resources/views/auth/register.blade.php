@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Registration</h1>
    </div>

<form class="form" method="POST" action="{{ route('register') }}">
    @csrf

    <label for="name" class="form__label">{{ __('Name') }}</label>
    <div class="form__input-wrapper">
        <input id="name" type="text" class="form__input form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

        @error('name')
            <div class="form__message-wrapper">
                <span class="form__message" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            </div>
        @enderror
    </div>

    <label for="email" class="form__label">{{ __('Email Address') }}</label>
    <div class="form__input-wrapper">
        <input id="email" type="email" class="form__input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

        @error('email')
            <div class="form__message-wrapper">
                <span class="form__message" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            </div>
        @enderror
    </div>

    <label for="password" class="form__label">{{ __('Password') }}</label>
    <div class="form__input-wrapper">
        <input id="password" type="password" class="form__input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

        @error('password')
            <div class="form__message-wrapper">
                <span class="form__message" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            </div>
        @enderror
    </div>

    <label for="password-confirm" class="form__label">{{ __('Confirm Password') }}</label>
    <div class="form__input-wrapper">
        <input id="password-confirm" type="password" class="form__input form-control" name="password_confirmation" required autocomplete="new-password">
    </div>

    <div class="form__btn-wrapper">
        <button type="submit" class="btn submit-btn">
            {{ __('Register') }}
        </button>
    </div>

    <div class="form__label">
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
    </div>
</form>
@endsection
