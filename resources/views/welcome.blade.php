<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Internship') }}</title>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <p class="msg">
                Start <br> your internship.
            </p>
        </div>
        <div class="main-content main-content__body">
            <div class="title__wrapper">
                <h1 class="title title_size-m">Press START and go to your tasks.</h1>
                <p class="subtitle subtitle_size-m">
                    You may have to log in if you have an account or create <br> a new one if you haven't had one before.
                </p>
            </div>

            <div class="main-content__btn-wrapper" >
                <a class="link success-btn" href="{{ route('login') }}">{{ __('START') }}</a>
            </div>
        </div>
        <footer class="footer">
            <div class="footer__information">© 2022 Best Internship. All Rights Reserved</div>
            <div class="footer__social-network">
            </div>
        </footer>
    </body>
</html>
