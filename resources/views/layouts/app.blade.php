<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name', 'Internship') }}</title>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
</head>
<body>
    <header class="header">
        <div class="navigation header__navigation">
            @guest
                @if (Route::has('login'))
                    <div class="navigation__item"><a class="navigation__link navigation__link_green" href="{{ route('login') }}">{{ __('Login') }}</a></div>
                @endif

                @if (Route::has('register'))
                    <div class="navigation__item"><a class="navigation__link navigation__link_green" href="{{ route('register') }}">{{ __('Register') }}</a></div>
                @endif
            @else
                <div class="navigation__item">{{ Auth::user()->name }}</div>

                <div class="navigation__item">
                    <a class="navigation__link navigation__link_red"
                       href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                    >
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>

                @auth
                    @if (Auth::user()->is_mentor == 0)
                        <div class="navigation__item"><a class="navigation__link navigation__link_green" href="/user/student">{{ __('Home Page') }}</a></div>
                    @else
                        <div class="navigation__item"><a class="navigation__link navigation__link_green" href="/user/mentor">{{ __('Home Page') }}</a></div>
                    @endif
                @endauth
            @endguest
                <div class="navigation__item"><a class="navigation__link navigation__link_green" href="/">{{ __('Start Page') }}</a></div>

        </div>
    </header>

    <div class="main-content">
        @yield('content')
    </div>

    <footer class="footer">
        <div class="footer__information">© 2022 Best Internship. All Rights Reserved</div>
    </footer>
</body>
</html>
