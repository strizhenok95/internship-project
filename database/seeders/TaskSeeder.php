<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Task, E-Commerce Basics',
                'start_date' => '2023-05-31',
                'mentor_description' => 'Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor ',
                'student_description' => 'Description for Student Description for Student Description for Student Description for Student Description for Student Description for Student ',
                'student_materials' => 'Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student ',
                'topic_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Task, We write code beautifully',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor ',
                'student_description' => 'Description for Student Description for Student Description for Student Description for Student Description for Student Description for Student ',
                'student_materials' => 'Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student ',
                'topic_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Task, Business logic and everything related to it',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor ',
                'student_description' => 'Description for Student Description for Student Description for Student Description for Student Description for Student Description for Student ',
                'student_materials' => 'Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student ',
                'topic_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Task, Application Testing',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor ',
                'student_description' => 'Description for Student Description for Student Description for Student Description for Student Description for Student Description for Student ',
                'student_materials' => 'Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student ',
                'topic_id' => 5,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Task, Model View Controller',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor Description for Mentor ',
                'student_description' => 'Description for Student Description for Student Description for Student Description for Student Description for Student Description for Student ',
                'student_materials' => 'Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student Materials for Student ',
                'topic_id' => 5,
            ]
        ]);
    }
}
