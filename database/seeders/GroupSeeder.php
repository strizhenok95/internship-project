<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Backend Developer 2022',
                'description' => 'Backend Developer Description Backend Developer Description Backend Developer Description Backend Developer Description Backend Developer Description Backend Developer Description ',
                'direction' => 'Backend',
                'start_date' => '2022-12-31 00:00:00',
                'end_date' => '2023-12-31 00:00:00',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Frontend Developer 2022',
                'description' => 'Frontend Developer Description Frontend Developer Description Frontend Developer Description Frontend Developer Description Frontend Developer Description Frontend Developer Description ',
                'direction' => 'Frontend',
                'start_date' => '2022-12-31 00:00:00',
                'end_date' => '2023-12-31 00:00:00',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'QA 2022',
                'description' => 'QA Description QA Description QA Description QA Description QA Description QA Description QA Description QA Description QA Description QA Description QA Description QA Description ',
                'direction' => 'QA',
                'start_date' => '2022-12-31 00:00:00',
                'end_date' => '2023-12-31 00:00:00',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Project Manager 2022',
                'description' => 'Project Manager Description Project Manager Description Project Manager Description Project Manager Description Project Manager Description Project Manager Description ',
                'direction' => 'Project Manager',
                'start_date' => '2022-12-31 00:00:00',
                'end_date' => '2023-12-31 00:00:00',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Business Analytics 2022',
                'description' => 'Business Analytics Description Business Analytics Description Business Analytics Description Business Analytics Description Business Analytics Description ',
                'direction' => 'Business Analytics',
                'start_date' => '2022-12-31 00:00:00',
                'end_date' => '2023-12-31 00:00:00',
            ]
        ]);
    }
}
