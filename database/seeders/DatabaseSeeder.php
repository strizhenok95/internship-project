<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GroupSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(TaskSeeder::class);
        $this->call(TopicSeeder::class);
        $this->call(ArticleUserSeeder::class);
        $this->call(TaskUserSeeder::class);
    }
}
