<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, ECommerce',
                'text' => 'E-commerce (electronic commerce) is the activity of electronically buying or selling of products on online services or over the Internet. E-commerce draws on technologies such as mobile commerce, electronic funds transfer, supply chain management, Internet marketing, online transaction processing, electronic data interchange (EDI), inventory management systems, and automated data collection systems. E-commerce is in turn driven by the technological advances of the semiconductor industry, and is the largest sector of the electronics industry.',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, Refactoring',
                'text' => 'Description Refactoring Description Refactoring Description Refactoring Description Refactoring Description Refactoring Description Refactoring',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, Business and Business',
                'text' => 'Description Business and Business Business and BusinessBusiness and BusinessBusiness and BusinessBusiness and BusinessBusiness and BusinessBusiness and Business',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, How to break what can not be broken',
                'text' => 'Description, How to break what can not be broken Description, How to break what can not be broken Description, How to break what can not be broken',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, All about MVC',
                'text' => 'Description, All about MVC. Description, All about MVC.  Description, All about MVC. Description, All about MVC. Description, All about MVC. Description, All about MVC.',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, New internet-shop',
                'text' => 'ENew internet-shop Description is the activity of electronically buying or selling of products on online ENew internet-shop Description, Internet marketing, online transaction processing, electronicENew internet-shop Description collection systems. E-commerce is in turn driven by the technological advances of the semiconductor industry, and is the largest sector of the electronics industry.',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, You and You',
                'text' => 'Description You and You Description You and You Description You and You Description You and You Description You and You Description You and You Description You and You Description You and You Description You and You ',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, All or Nothing',
                'text' => 'Description All or Nothing Description All or Nothing Description All or Nothing Description All or Nothing Description All or Nothing Description All or Nothing Description All or Nothing Description All or Nothing ',
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Article, How Fast Make and Good',
                'text' => 'Description How Fast Make and Good Description How Fast Make and Good Description How Fast Make and Good Description How Fast Make and Good Description How Fast Make and Good Description How Fast Make and Good Description How Fast Make and Good ',
            ],
        ]);
    }
}
