<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ArticleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article_user')->insert([
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 1,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 2,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 3,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 4,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 5,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 1,
                'user_id' => 2,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 2,
                'user_id' => 2,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 3,
                'user_id' => 3,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 4,
                'user_id' => 2,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 5,
                'user_id' => 3,
            ], [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 6,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 7,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 8,
                'user_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'article_id' => 9,
                'user_id' => 1,
            ],
        ]);
    }
}
