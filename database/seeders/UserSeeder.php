<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Mentor Ivan',
                'is_mentor' => 1,
                'email' => 'mentor@gmail.com',
                'description' => 'I really love programming and dreamed of becoming a backend developer. I study at the Southern Federal University at the Department of MOP EVM.',
                'feedback' => 'Good Mentor',
                'birthday' => '2000-06-17',
                'password' => Hash::make('12345678'),
                'group_id' => '1',
            ],
            [
                'name' => 'Matthew Swift',
                'is_mentor' => 0,
                'email' => 'strizhenok@gmail.com',
                'description' => 'I study at the Southern Federal University at the Department of MOP EVM. I really love programming and dreamed of becoming a backend developer.',
                'feedback' => 'The intern successfully completed the internship. Develops on the project, is able to solve small problems independently.',
                'birthday' => '2000-06-12',
                'password' => Hash::make('12345678'),
                'group_id' => '1',
            ],
            [
                'name' => 'Alex',
                'is_mentor' => 0,
                'email' => 'alex@gmail.com',
                'description' => 'I study at the Southern Federal University at the Department of MOP EVM. I really love programming and dreamed of becoming a backend developer.',
                'feedback' => 'The intern successfully completed the internship. Develops on the project, is able to solve small problems independently.',
                'birthday' => '2000-08-21',
                'password' => Hash::make('12345678'),
                'group_id' => '1',
            ],
            [
                'name' => 'Serega',
                'is_mentor' => 0,
                'email' => 'serega@gmail.com',
                'description' => 'I study at the Southern Federal University at the Department of MOP EVM. I really love programming and dreamed of becoming a backend developer.',
                'feedback' => 'The intern successfully completed the internship. Develops on the project, is able to solve small problems independently.',
                'birthday' => '2000-08-21',
                'password' => Hash::make('12345678'),
                'group_id' => '1',
            ],
            [
                'name' => 'Ura',
                'is_mentor' => 0,
                'email' => 'ura@gmail.com',
                'description' => 'I study at the Southern Federal University at the Department of MOP EVM. I really love programming and dreamed of becoming a backend developer.',
                'feedback' => 'The intern successfully completed the internship. Develops on the project, is able to solve small problems independently.',
                'birthday' => '2000-08-21',
                'password' => Hash::make('12345678'),
                'group_id' => '1',
            ],
            [
                'name' => 'Lera',
                'is_mentor' => 0,
                'email' => 'lera@gmail.com',
                'description' => 'I study at the Southern Federal University at the Department of MOP EVM. I really love programming and dreamed of becoming a backend developer.',
                'feedback' => 'The intern successfully completed the internship. Develops on the project, is able to solve small problems independently.',
                'birthday' => '2000-08-21',
                'password' => Hash::make('12345678'),
                'group_id' => '1',
            ],
            [
                'name' => 'LeraL',
                'is_mentor' => 0,
                'email' => 'leraa@gmail.com',
                'description' => 'I study at the Southern Federal University at the Department of MOP EVM. I really love programming and dreamed of becoming a backend developer.',
                'feedback' => 'The intern successfully completed the internship. Develops on the project, is able to solve small problems independently.',
                'birthday' => '2000-08-21',
                'password' => Hash::make('12345678'),
                'group_id' => '2',
            ],
        ]);
    }
}
