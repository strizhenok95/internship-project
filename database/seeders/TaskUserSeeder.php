<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TaskUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task_user')->insert([
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 70,
                'user_id' => 1,
                'task_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => null,
                'user_id' => 1,
                'task_id' => 2,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 80,
                'user_id' => 1,
                'task_id' => 3,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => null,
                'user_id' => 1,
                'task_id' => 4,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 90,
                'user_id' => 1,
                'task_id' => 5,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 88,
                'user_id' => 2,
                'task_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 70,
                'user_id' => 3,
                'task_id' => 2,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 70,
                'user_id' => 2,
                'task_id' => 3,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 79,
                'user_id' => 2,
                'task_id' => 4,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'mark' => 77,
                'user_id' => 4,
                'task_id' => 5,
            ],
        ]);
    }
}
