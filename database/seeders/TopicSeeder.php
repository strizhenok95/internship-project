<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topics')->insert([
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Git, distributed version control system',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Mentor Description Mentor Description Mentor Description Mentor Description ',
                'student_description' => 'Student Description Student Description Student Description Student Description ',
                'group_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Database, Mysql',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Mentor Description Mentor Description Mentor Description Mentor Description ',
                'student_description' => 'Student Description Student Description Student Description Student Description ',
                'group_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Fundamentals of PHP, PhpStorm',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Mentor Description Mentor Description Mentor Description Mentor Description ',
                'student_description' => 'Student Description Student Description Student Description Student Description ',
                'group_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'HTML, Hypertext Markup Language',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Mentor Description Mentor Description Mentor Description Mentor Description ',
                'student_description' => 'Student Description Student Description Student Description Student Description ',
                'group_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Business logic and everything related to it',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Mentor Description Mentor Description Mentor Description Mentor Description ',
                'student_description' => 'Student Description Student Description Student Description Student Description ',
                'group_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Application Testing',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Mentor Description Mentor Description Mentor Description Mentor Description ',
                'student_description' => 'Student Description Student Description Student Description Student Description ',
                'group_id' => 1,
            ],
            [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'name' => 'Model View Controller',
                'start_date' => '2023-12-31',
                'mentor_description' => 'Mentor Description Mentor Description Mentor Description Mentor Description ',
                'student_description' => 'Student Description Student Description Student Description Student Description ',
                'group_id' => 2,
            ]
        ]);
    }
}
