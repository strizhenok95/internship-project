<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

//    protected function redirectTo()
//    {
//        if (Auth::user()->isMentor()) {
//            return route('user/mentor');
//        } else {
//            return route('user/student');
//        }
//    }

    protected function authenticated(Request $request, $user)
    {
        if ( $user->isMentor() ) {// do your magic here
            return redirect('user/mentor');
        }

        return redirect('user/student');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
