<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'start_date',
        'mentor_description',
        'student_description',
        'student_materials',
        'topic_id',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
