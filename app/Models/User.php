<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;
use Modules\Group\Entities\Group;

class User extends Authenticatable
{
    protected const MENTOR_CODE = 1;

    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    public function isMentor()
    {
        return $this->is_mentor === self::MENTOR_CODE;
    }

    public function studentsByGroupId($groupId)
    {
        return DB::table('users')
            ->where('group_id', $groupId)
            ->where('is_mentor', 0);
    }

    public function mentorsByGroupId($groupId)
    {
        return DB::table('users')
            ->where('group_id', $groupId)
            ->where('is_mentor', 1)
            ->get();
    }

    public function updateFeedback($user, $feedback)
    {
        return DB::table('users')
            ->where('id', $user->id)
            ->update(['feedback' => $feedback]);
    }
}
