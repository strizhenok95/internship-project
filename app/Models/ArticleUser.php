<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleUser extends Model
{
    use HasFactory;

    protected $table = 'article_user';

    protected $fillable = [
        'article_id',
        'user_id',
    ];
}
