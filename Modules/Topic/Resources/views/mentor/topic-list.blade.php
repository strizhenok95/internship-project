@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Topics</h1>
    </div>
    <div class="main-navigation">
        <a class="main-navigation__link main-navigation__link_pl-10" href="{{ route('topic-mentor-form') }}">
            <div class="main-navigation__item main-navigation__item_white main-navigation__item_bg-green">
                Add new Topic
            </div>
        </a>
        @foreach($topics as $topic)
            <a class="main-navigation__link main-navigation__link_green" href="{{ route('topic-mentor', $topic->id) }}">
                <div class="main-navigation__item">
                    {{ $topic->name }}
                </div>
            </a>
        @endforeach
        <div class="main-navigation__pagination">
            {{ $topics->links() }}
        </div>
    </div>
@endsection
