@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Form for adding a new Topic</h1>
    </div>

    <form class="form" method="POST" action="{{ route('topic-mentor-store') }}">
        @csrf

        <label for="name" class="form__label">{{ __('Name') }}</label>
        <div class="form__input-wrapper">
            <input id="name" type="name" class="form__input" name="name" required autofocus>
        </div>

        <label for="start-date" class="form__label">{{ __('Start Date') }}</label>
        <div class="form__input-wrapper">
            <input id="start-date" type="date" class="form__input" name="start-date" required autofocus>
        </div>

        <label for="mentor_description" class="form__label">{{ __('Description for Mentor') }}</label>
        <div class="form__input-wrapper">
            <textarea id="mentor_description" type="text" class="form__input form__input_size-xl" name="mentor-description" required autofocus></textarea>
        </div>

        <label for="student_description" class="form__label">{{ __('Description for Student') }}</label>
        <div class="form__input-wrapper">
            <textarea id="student_description" type="text" class="form__input form__input_size-xl" name="student-description" required autofocus></textarea>
        </div>

        <div class="form__btn-wrapper">
            <button type="submit" class="link submit-btn">
                {{ __('Add Topic') }}
            </button>
        </div>
    </form>
@endsection
