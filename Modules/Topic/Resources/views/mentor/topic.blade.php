@extends('layouts.app')

@section('content')

    <div class="title__wrapper">
        <h1 class="title title_size-l">Tasks</h1>
    </div>
    <div class="main-navigation">
        <a class="main-navigation__link main-navigation__link_pl-10" href="{{ route('task-mentor-form', $topicId) }}">
            <div class="main-navigation__item main-navigation__item_white main-navigation__item_bg-green">
                Add new Task
            </div>
        </a>

        @foreach($tasks as $task)
            @if ($topicId == $task->topic_id)
                <a class="main-navigation__link main-navigation__link_green" href="{{ route('task-mentor', $task->id) }}">
                    <div class="main-navigation__item">
                        {{ $task->name }}
                    </div>
                </a>
            @endif
        @endforeach
        <div class="main-navigation__pagination">
            {{ $tasks->links() }}
        </div>
    </div>
@endsection
