@extends('layouts.app')

@section('content')

    <div class="title__wrapper">
        <h1 class="title title_size-l">Tasks</h1>
    </div>
    <div class="main-navigation">
        @foreach($tasks as $task)
            @if ($topicId == $task->topic_id)
                <a class="main-navigation__link main-navigation__link_green" href="{{ route('task-student', $task->id) }}">
                    <div class="main-navigation__item">
                        {{ $task->name }}
                    </div>
                </a>
            @endif
        @endforeach
        <div class="main-navigation__pagination">
            {{ $tasks->links() }}
        </div>
    </div>
@endsection
