<?php

namespace Modules\Topic\Http\Controllers\Mentor;

use App\Models\Topic;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Group\Entities\Group;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $user = auth()->user();
        $group = Group::find($user->group_id);
        $topics = $group->topics()->simplePaginate(5);
        return view('topic::mentor.topic-list', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('topic::mentor.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $groupId = $user->group_id;

        $params = $request->all();
        $params += ['group_id' => $groupId];

        Topic::create($params);
        return redirect()->route('topic-mentor-list');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($topicId)
    {
        $user = auth()->user();
        $tasks = $user->tasks()->simplePaginate(6);
        return view('topic::mentor.topic', compact('tasks', 'topicId'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('topic::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
