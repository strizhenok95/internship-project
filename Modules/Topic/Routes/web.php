<?php

use Illuminate\Support\Facades\Route;
use Modules\Topic\Http\Controllers\Mentor\TopicController as MentorTopicController;
use Modules\Topic\Http\Controllers\Student\TopicController as StudentTopicController;

Route::prefix('topic')->group(function() {
    Route::get('/mentor-list', [MentorTopicController::class, 'index'])->name('topic-mentor-list')->middleware('mentor');
    Route::get('/mentor/form', [MentorTopicController::class, 'create'])->name('topic-mentor-form')->middleware('mentor');
    Route::get('/mentor/{id}', [MentorTopicController::class, 'show'])->name('topic-mentor')->middleware('mentor');
    Route::post('/mentor/store', [MentorTopicController::class, 'store'])->name('topic-mentor-store')->middleware('mentor');

    Route::get('/student-list', [StudentTopicController::class, 'index'])->name('topic-student-list')->middleware('student');
    Route::get('/student/{id}', [StudentTopicController::class, 'show'])->name('topic-student')->middleware('student');
});
