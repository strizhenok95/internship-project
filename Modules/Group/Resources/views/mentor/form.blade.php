@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Form for adding a new Feedback</h1>
    </div>

    <div class="content">
        <div class="content__article-item">
            Existing Feedback: {{ $student->feedback }}
        </div>
    </div>

    <form class="form" method="POST" action="{{ route('group-mentor-store', $student->id) }}">
        @csrf

        <label for="feedback" class="form__label">{{ __('Feedback') }}</label>
        <div class="form__input-wrapper">
            <textarea id="feedback" type="text" class="form__input form__input_size-xl" name="feedback" required autofocus></textarea>
        </div>

        <div class="form__btn-wrapper">
            <button type="submit" class="link submit-btn">
                {{ __('Add Feedback') }}
            </button>
        </div>
    </form>
@endsection
