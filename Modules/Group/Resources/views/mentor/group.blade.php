@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Group students</h1>
    </div>
    <div class="main-navigation">
        @foreach($students as $student)
            <a class="main-navigation__link main-navigation__link_green" href="{{ route('group-mentor-form', $student->id) }}">
                <div class="main-navigation__item">
                    {{ $student->name }} <br>
                    {{ $student->email }}
                </div>
            </a>
        @endforeach
        <div class="main-navigation__pagination">
            {{ $students->links() }}
        </div>
    </div>
@endsection
