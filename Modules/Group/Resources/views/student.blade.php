@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">{{ $group->name }}</h1>
    </div>
    <div class="main-navigation">
        <div class="main-navigation__wrapper">
            <div class="main-navigation__item main-navigation__item_white">
                Direction: {{ $group->direction }}
            </div>
            <div class="main-navigation__item main-navigation__item_white">
                Start Date: {{ $group->start_date }}
            </div>
            <div class="main-navigation__item main-navigation__item_white">
                End Date: {{ $group->end_date }}
            </div>
        </div>
    </div>

    <div class="content">
        <div class="content__article-item main-navigation__item_white">
            {{ $group->description }}
        </div>
    </div>
@endsection
