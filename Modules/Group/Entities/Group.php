<?php

namespace Modules\Group\Entities;

use App\Models\Task;
use App\Models\Topic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Group extends Model
{
    use HasFactory;

    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
