<?php

use Illuminate\Support\Facades\Route;
use Modules\Group\Http\Controllers\Student\GroupController as StudentController;
use Modules\Group\Http\Controllers\Mentor\GroupController as MentorController;

Route::prefix('group')->group(function() {
    Route::get('/mentor', [MentorController::class, 'index'])->name('group-mentor')->middleware('mentor');
    Route::get('/mentor/form/{id}', [MentorController::class, 'create'])->name('group-mentor-form')->middleware('mentor');
    Route::post('/mentor/store/{id}', [MentorController::class, 'store'])->name('group-mentor-store')->middleware('mentor');
    Route::get('/student', [StudentController::class, 'index'])->name('group-student')->middleware('student');
});
