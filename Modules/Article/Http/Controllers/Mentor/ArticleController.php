<?php

namespace Modules\Article\Http\Controllers\Mentor;

use App\Models\Article;
use App\Models\ArticleUser;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $articles = $user->articles()->simplePaginate(5);
        return view('article::mentor.article-list', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('article::mentor.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $article = Article::create($params);

        $user = auth()->user();
        $groupId = $user->group_id;
        $students = $user->studentsByGroupId($groupId)->get();
        $mentors = $user->mentorsByGroupId($groupId);

        $this->createArticleUser($students, $article);
        $this->createArticleUser($mentors, $article);

        return redirect()->route('article-mentor-list');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('article::mentor.article', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('article::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    protected function createArticleUser($users, $article)
    {
        foreach ($users as $user) {
            ArticleUser::create([
                'article_id' => $article->id,
                'user_id' => $user->id,
            ]);
        }
    }
}
