@extends('layouts.app')

@section('content')

    <div class="title__wrapper">
        <h1 class="title title_size-l">Articles</h1>
    </div>
    <div class="main-navigation">
        @foreach($articles as $article)
            <a class="main-navigation__link main-navigation__link_green" href="{{ route('article-student', $article->id) }}">
                <div class="main-navigation__item">
                    {{ $article->name }}
                </div>
            </a>
        @endforeach
        <div class="main-navigation__pagination">
            {{ $articles->links() }}
        </div>
    </div>
@endsection
