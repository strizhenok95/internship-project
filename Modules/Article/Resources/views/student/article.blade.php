@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">{{ $article->name }}</h1>
    </div>
    <div class="main-navigation">
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('article-student-list') }}">
            <div class="main-navigation__item">
                Articles
            </div>
        </a>
    </div>

    <div class="content">
        <div class="content__article-item">
            {{ $article->text }}
        </div>
    </div>
@endsection
