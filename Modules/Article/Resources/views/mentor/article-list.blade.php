@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Articles</h1>
    </div>
    <div class="main-navigation">
        <a class="main-navigation__link" href="{{ route('article-mentor-form') }}">
            <div class="main-navigation__item main-navigation__item_white main-navigation__item_bg-green">
                Add new Article
            </div>
        </a>

        @foreach($articles as $article)
            <a class="main-navigation__link main-navigation__link_green" href="{{ route('article-mentor', $article->id) }}">
                <div class="main-navigation__item">
                    {{ $article->name }}
                </div>
            </a>
        @endforeach
        <div class="main-navigation__pagination">
            {{ $articles->links() }}
        </div>
    </div>
@endsection
