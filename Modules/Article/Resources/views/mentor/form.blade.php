@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Form for adding a new Article</h1>
    </div>

    <form class="form" method="POST" action="{{ route('article-mentor-store') }}">
        @csrf

        <label for="name" class="form__label">{{ __('Name') }}</label>
        <div class="form__input-wrapper">
            <input id="name" type="name" class="form__input" name="name" required autofocus>
        </div>

        <label for="text" class="form__label">{{ __('Text') }}</label>
        <div class="form__input-wrapper">
            <textarea id="text" type="text" class="form__input form__input_size-xl" name="text" required autofocus></textarea>
        </div>

        <div class="form__btn-wrapper">
            <button type="submit" class="link submit-btn">
                {{ __('Add Article') }}
            </button>
        </div>
    </form>
@endsection
