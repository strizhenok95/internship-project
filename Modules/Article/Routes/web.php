<?php

use Illuminate\Support\Facades\Route;
use Modules\Article\Http\Controllers\Mentor\ArticleController as MentorArticleController;
use Modules\Article\Http\Controllers\Student\ArticleController as StudentArticleController;

Route::prefix('article')->group(function() {
    Route::get('/mentor-list', [MentorArticleController::class, 'index'])->name('article-mentor-list')->middleware('mentor');
    Route::get('/mentor/form', [MentorArticleController::class, 'create'])->name('article-mentor-form')->middleware('mentor');
    Route::post('/mentor/store', [MentorArticleController::class, 'store'])->name('article-mentor-store')->middleware('mentor');
    Route::get('/mentor/{id}', [MentorArticleController::class, 'show'])->name('article-mentor')->middleware('mentor');

    Route::get('/student-list', [StudentArticleController::class, 'index'])->name('article-student-list')->middleware('student');
    Route::get('/student/{id}', [StudentArticleController::class, 'show'])->name('article-student')->middleware('student');
});
