<?php

namespace Modules\Task\Http\Controllers\Mentor;

use App\Models\Task;
use App\Models\TaskUser;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use function auth;
use function view;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('task::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($topicId)
    {
        return view('task::mentor.form', compact('topicId'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $task = Task::create($params);

        $user = auth()->user();
        $groupId = $user->group_id;
        $students = $user->studentsByGroupId($groupId)->get();
        $mentors = $user->mentorsByGroupId($groupId);

        $this->createTaskUser($students, $task);
        $this->createTaskUser($mentors, $task);

        return redirect()->route('topic-mentor-list');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $task = Task::find($id);
        $user = auth()->user();
        $taskUser = DB::table('task_user')->where('user_id', $user->id)->where('task_id', $id)->first();
        return view('task::mentor.task', compact('task', 'taskUser'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('task::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    protected function createTaskUser($users, $article)
    {
        foreach ($users as $user) {
            TaskUser::create([
                'task_id' => $article->id,
                'user_id' => $user->id,
            ]);
        }
    }
}
