<?php

use Illuminate\Support\Facades\Route;
use Modules\Task\Http\Controllers\Mentor\TaskController as MentorTaskController;
use Modules\Task\Http\Controllers\Student\TaskController as StudentTaskController;

Route::prefix('task')->group(function() {
    Route::get('/mentor/form/{topicId}', [MentorTaskController::class, 'create'])->name('task-mentor-form')->middleware('mentor');
    Route::post('/mentor/store', [MentorTaskController::class, 'store'])->name('task-mentor-store')->middleware('mentor');
    Route::get('/mentor/{id}', [MentorTaskController::class, 'show'])->name('task-mentor')->middleware('mentor');

    Route::get('/student/{id}', [StudentTaskController::class, 'show'])->name('task-student')->middleware('student');
});
