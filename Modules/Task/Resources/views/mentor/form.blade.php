@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Form for adding a new Task</h1>
    </div>

    <form class="form" method="POST" action="{{ route('task-mentor-store') }}">
        @csrf

        <div class="form__input-wrapper">
            <input type="hidden" id="topic_id" type="text" class="form__input" name="topic_id" value="{{ $topicId }}" required autofocus>
        </div>

        <label for="name" class="form__label">{{ __('Name') }}</label>
        <div class="form__input-wrapper">
            <input id="name" type="text" class="form__input" name="name" required autofocus>
        </div>

        <label for="start_date" class="form__label">{{ __('Start Date') }}</label>
        <div class="form__input-wrapper">
            <input id="start_date" type="datetime-local" class="form__input" name="start_date" required autofocus>
        </div>

        <label for="mentor_description" class="form__label">{{ __('Description for Mentor') }}</label>
        <div class="form__input-wrapper">
            <textarea id="mentor_description" type="text" class="form__input form__input_size-xl" name="mentor_description" required autofocus></textarea>
        </div>

        <label for="student_description" class="form__label">{{ __('Description for Student') }}</label>
        <div class="form__input-wrapper">
            <textarea id="student_description" type="text" class="form__input form__input_size-xl" name="student_description" required autofocus></textarea>
        </div>

        <label for="student_materials" class="form__label">{{ __('Materials for Student') }}</label>
        <div class="form__input-wrapper">
            <textarea id="student_materials" type="text" class="form__input form__input_size-xl" name="student_materials" required autofocus></textarea>
        </div>

        <div class="form__btn-wrapper">
            <button type="submit" class="link submit-btn">
                {{ __('Add Task') }}
            </button>
        </div>
    </form>
@endsection
