@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">{{ $task->name }}</h1>
    </div>
    <div class="main-navigation">
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('topic-mentor-list') }}">
            <div class="main-navigation__item">
                Topics
            </div>
        </a>
        <a class="main-navigation__link main-navigation__link_green">
            <div class="main-navigation__item">
                Start Date: {{ $task->start_date ? $task->start_date : "?" }}
            </div>
        </a>
    </div>

    <div class="content">
        <div class="content__article-item">
            {{ $task->student_description }}
        </div>
        <div class="content__article-item">
            {{ $task->student_materials }}
        </div>
    </div>
@endsection
