<?php

namespace Modules\User\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use function auth;
use function view;

class PersonalAreaController extends Controller
{
    public function index() {
        $user = auth()->user();
        return view('user::student.personal-area', compact('user'));
    }
}
