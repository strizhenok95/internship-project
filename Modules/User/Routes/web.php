<?php

use Illuminate\Support\Facades\Route;
use Modules\User\Http\Controllers\Student\MainController as StudentMainController;
use Modules\User\Http\Controllers\Mentor\MainController as MentorMainController;
use Modules\User\Http\Controllers\Student\PersonalAreaController;

Route::prefix('user')->group(function() {
    Route::middleware(['auth'])->group(function() {
        Route::get('/mentor', [MentorMainController::class, 'index'])->name('mentor')->middleware('mentor');
        Route::get('/student-personal-area', [PersonalAreaController::class, 'index'])->name('student-personal-area')->middleware('student');
        Route::get('/student', [StudentMainController::class, 'index'])->name('student')->middleware('student');
    });
});


