@extends('layouts.app')

@section('content')

    <div class="title__wrapper">
        <h1 class="title title_size-l">Mentor Home Page</h1>
    </div>
    <div class="main-navigation">
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('topic-mentor-list') }}">
            <div class="main-navigation__item ">
                Topics
            </div>
        </a>
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('article-mentor-list') }}">
            <div class="main-navigation__item ">
                Articles
            </div>
        </a>
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('group-mentor') }}">
            <div class="main-navigation__item">
                Group of students
            </div>
        </a>
    </div>
@endsection
