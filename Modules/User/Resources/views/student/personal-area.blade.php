@extends('layouts.app')

@section('content')
    <div class="title__wrapper">
        <h1 class="title title_size-l">Student Personal Area</h1>
    </div>
    <div class="main-navigation">
        <div class="main-navigation__wrapper">
            <div class="main-navigation__item main-navigation__item_white">
                Name: {{ $user->name }}
            </div>
            <div class="main-navigation__item main-navigation__item_white">
                Birthday: {{ $user->birthday }}
            </div>
        </div>
    </div>

    <div class="content">
        <div class="content__article-item main-navigation__item_white">
            Student Information: {{ $user->description }}
        </div>
    </div>
    <div class="content">
        <div class="content__article-item main-navigation__item_white">
            Feedback Information: {{ $user->feedback }}
        </div>
    </div>
@endsection
