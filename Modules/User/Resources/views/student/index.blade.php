@extends('layouts.app')

@section('content')

    <div class="title__wrapper">
        <h1 class="title title_size-l">Student Home Page</h1>
    </div>
    <div class="main-navigation">
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('topic-student-list') }}">
            <div class="main-navigation__item ">
                Topics
            </div>
        </a>
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('article-student-list') }}">
            <div class="main-navigation__item ">
                Articles
            </div>
        </a>
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('group-student') }}">
            <div class="main-navigation__item">
                My Group
            </div>
        </a>
        <a class="main-navigation__link main-navigation__link_green" href="{{ route('student-personal-area') }}">
            <div class="main-navigation__item">
                Personal Area
            </div>
        </a>
    </div>
@endsection
